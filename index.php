<?php

include 'koneksi.php';

$data_absen = mysqli_query($conn, "SELECT * FROM tb_absen");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <title>Qr Code</title>
    <style>
        html, body, canvas {
            width: 100%;
            height: 100%;
        }
    </style>
</head>

<body>
    <!-- Navbar -->

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="#">Absen</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Page 1</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Page 2</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Page 3</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Akhir Navbar -->

    <section>
        <div class="container mt-5 ">
            <div class="row">
                <div class="col-md-5">
                    <!-- <video id="preview" width="100%"></video> -->
                    <canvas hidden="" id="qr-canvas"></canvas>
                </div>
                <div class="col-md-7">
                    <form action="system.php" method="post" class="form-horizontal">
                        <label>Scan QR CODE</label>
                        <input type="hidden" id="id_absen" name="id_absen">
                        <div id="qr-result">
                            <input type="text" name="text" id="outputData" readonly="" placeholder="Scan QR CODE" class="form-control">
                        </div>
                    </form>
                    <div class="table-responsive mt-3">
                        <table class="table table-dark table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Nama Siswa</th>
                                    <th scope="col">Jam Masuk</th>
                                    <th scope="col">Jam Keluar</th>
                                    <th scope="col">Tanggal Masuk</th>
                                    <th scope="col">STATUS</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                <?php foreach ($data_absen as $absen) : ?>
                                    <tr>
                                        <th scope="row"><?= $no++; ?></th>
                                        <td><?= $absen['nama_siswa']; ?></td>
                                        <td><?= $absen['jam_masuk']; ?></td>
                                        <td><?= $absen['jam_keluar']; ?></td>
                                        <td><?= $absen['tanggal_masuk']; ?></td>
                                        <td><?= $absen['STATUS']; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="js/instascan.min.js"></script>
    <script src="js/vue.min.js"></script>
    <script src="js/adapter.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/qr_packed.js"></script>

    <script type="module">

        const qrcode = window.qrcode;

        const video = document.createElement("video");
        const canvasElement = document.getElementById("qr-canvas");
        const canvas = canvasElement.getContext("2d");
        const qrResult = document.getElementById("qr-result");
        const outputData = document.getElementById("outputData");

        let scanning = false;

        qrcode.callback = (res) => {
            if(res) {
                outputData.value = res;

                scanning = false;
                video.srcObject.getTracks().forEach((track) => {
                    // track.stop();
                    document.forms[0].submit();
                } );
                canvasElement.hidden = true;
            }
        }

        navigator.mediaDevices.getUserMedia({ video: { facingMode: "environment" } }).then(function (stream) {
            scanning = true;
            canvasElement.hidden = false;
            video.setAttribute("playsinline", true); // ios
            video.srcObject = stream;
            video.play();
            tick();
            scan();
        });

        function tick() {
          canvasElement.height = video.videoHeight;
          canvasElement.width = video.videoWidth;
          canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);

          scanning && requestAnimationFrame(tick);
        }

        function scan() {
            try {
                qrcode.decode();
            } catch (e) {
                setTimeout(scan, 300);
            }
        }

        // let scanner = new Instascan.Scanner({
        //     video: document.getElementById('preview')
        // });
        // Instascan.Camera.getCameras().then(function(cameras) {
        //     if (cameras.length > 0) {
        //         scanner.start(cameras[0]);
        //     } else {
        //         alert('No cameras found');
        //     }
        // }).catch(function(e) {
        //     console.error(e);
        // });

        // scanner.addListener('scan', function(c) {
        //     document.getElementById('text').value = c;
        //     document.forms[0].submit();
        // });
    </script>
</body>

</html>